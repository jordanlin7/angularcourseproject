import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';
import { Recipe } from './recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  recipeChanged = new Subject<Recipe[]>();

  recipeSelected = new Subject<Recipe>();

  // 一堆食譜
  recipes: Recipe[] = [];
  /*
  [
    new Recipe(
      'A Test Recipe',
      'This is simply a test',
      'https://images.immediate.co.uk/production/volatile/sites/30/2013/05/Puttanesca-fd5810c.jpg',
      [
        new Ingredient('Meat', 1),
        new Ingredient('Fish', 5)
      ]
    ),
    new Recipe(
      'Recipe 2',
      'Something to eat',
       'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg',
       [
        new Ingredient('Banana', 3),
        new Ingredient('Beef', 10)
       ])
  ];*/

  constructor() { }

  getRecipes() {
    // .slice()會回傳一個copy，如果直接傳回recipes的話，
    // 等於是直接把service內的recipes reference回傳，這樣只要在外部修改就會一併動到service中的recipes
    return this.recipes.slice();
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipeChanged.next(this.recipes.slice());
  }

  getRecipe(id : number) {
    return this.recipes[id];
  }

  addRecipe(recipe : Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice())

  }

  updateRecipe(index:number, newRecipe:Recipe) {
    this.recipes[index] = newRecipe;
    this.recipeChanged.next(this.recipes.slice())
  }

  deleteRecipe(index:number) {
    this.recipes.splice(index, 1);
    this.recipeChanged.next(this.recipes.slice())
  }


}
