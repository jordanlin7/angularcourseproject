import { RecipeService } from './../recipe.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id! : number;
  editMode = false;
  recipeForm! : FormGroup;

  constructor(private route : ActivatedRoute,
              private fb : FormBuilder,
              private recipeService : RecipeService,
              private router : Router) { }

  ngOnInit(): void {
    this.route.params.subscribe( (params:Params)=> {
      this.id = params['id'];
      this.editMode = params['id'] != undefined;
      this.initForm();
    })
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route})
  }

  private initForm() {
    let recipeName = '';
    let recipeImagePath = '';
    let recipeDescription = '';
    let recipeIngredients = this.fb.array([]);

    if(this.editMode) {
      const recipe = this.recipeService.getRecipe(this.id);
      recipeName = recipe.name;
      recipeImagePath = recipe.imagePath;
      recipeDescription = recipe.description;

      if(recipe['ingredients']) {
        for(let ingredient of recipe.ingredients) {
          recipeIngredients.push(
            this.fb.group({
              'name' : this.fb.control(ingredient.name, Validators.required),
              'amount' : this.fb.control(ingredient.amount, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
            })
          )
        }
      }
    }

    this.recipeForm = this.fb.group({
      'name' : this.fb.control(recipeName, Validators.required),
      'imagePath' : this.fb.control(recipeImagePath, Validators.required),
      'description' : this.fb.control(recipeDescription, Validators.required),
      'ingredients' : recipeIngredients
    })
  }

  get controls() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }


  onSubmit() {
    // 因為reactive form跟我們需要的Recipe格式應該是一樣的，所以我們可以直接把recipeForm.value傳過去
    // const newRecipe : Recipe = new Recipe(
    //   this.recipeForm.value['name'],
    //   this.recipeForm.value['description'],
    //   this.recipeForm.value['imagePath'],
    //   this.recipeForm.value['ingredients']
    // )

    if(this.editMode) {
      this.recipeService.updateRecipe(this.id, /*newRecipe*/ this.recipeForm.value)
    }
    else {
      this.recipeService.addRecipe(/*newRecipe*/this.recipeForm.value);
    }

    this.onCancel();
  }

  onAddIngredient() {
    // 這邊前面要轉型成FormArray是因為typescript並不知道我們這個是一個FormArray
    (<FormArray>this.recipeForm.get('ingredients')).push(
      this.fb.group({
        'name' : this.fb.control('', Validators.required),
        'amount' : this.fb.control('', [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    )

  }

  onDeleteIngredient(index:number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }




}
