import { RecipeService } from './../recipe.service';
import { ShoppingListService } from './../../shopping-list/shopping-list.service';
import { Recipe } from './../recipe.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  //@Input() selectedRecipe!:Recipe;

  selectedRecipe!:Recipe;
  id!:number;


  constructor(private shoppinglistService : ShoppingListService,
              private route : ActivatedRoute,
              private recipeservice : RecipeService,
              private router : Router) { }

  ngOnInit(): void {

    this.route.params.subscribe( (params: Params)=> {
      this.id = +params['id'];
      this.selectedRecipe = this.recipeservice.getRecipe(this.id);
    })
  }

  onAddToShoppingList() {
    this.shoppinglistService.addIngredientsToShoppingList(this.selectedRecipe.ingredients);
  }

  onEditRecipe() {
    // 我們已經在.../id這條路線上，所以只要加上edit就好
    this.router.navigate(['edit'], {relativeTo:this.route})

  }

  onDeleteRecipe() {
    this.recipeservice.deleteRecipe(this.id);
    this.router.navigate(['/recipes'])

  }
}
