import { Ingredient } from "../shared/ingredient.model";

export class Recipe {
  public name: string;              // 食譜名稱
  public description: string;       // 食譜描述
  public imagePath: string;         // 食譜圖路徑
  public ingredients: Ingredient[]; // 成份

  constructor(name: string, desc: string, imgPath: string, ingredients:Ingredient[]) {
    this.name = name;
    this.description = desc;
    this.imagePath = imgPath;
    this.ingredients = ingredients;
  }
}
