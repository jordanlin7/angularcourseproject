import { DataStorageService } from './../shared/data-storage.service';
import { Component, EventEmitter, Output } from "@angular/core";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',

})
export class HeaderComponent {
@Output() pageselect = new EventEmitter<string>();

  constructor(private dataStorage : DataStorageService){

  }

  onSelect(selpage : string) {
    this.pageselect.emit(selpage)

  }

  onSaveData() {
    this.dataStorage.storeRecipes();
  }

  onFetchData() {
    this.dataStorage.fetchRecipes().subscribe();
  }




}
