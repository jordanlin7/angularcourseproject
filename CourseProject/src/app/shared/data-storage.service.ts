import { RecipeService } from './../recipes/recipe.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Recipe } from '../recipes/recipe.model';
import { map, tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private http: HttpClient, private recipesService: RecipeService) { }

  storeRecipes() {
    const recipes = this.recipesService.getRecipes();


    this.http.put('https://courseproject-17dc8-default-rtdb.firebaseio.com/recipes.json', recipes).subscribe(response=> {
      console.log(response);
    })

  }

  fetchRecipes() {
    return this.http.get<Recipe[]>('https://courseproject-17dc8-default-rtdb.firebaseio.com/recipes.json')
    .pipe(map(recipes => {  // 這個map是rxjs的map
      return recipes.map(recipe => { // 這個map是javascript array裡的map
        return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []}; // 這邊去看看是否有ingredients，如果沒有的話，加一個空的進去，不然會出錯
      })
    }),
    tap(recipes=> {
      this.recipesService.setRecipes(recipes);
    })
    )


  }




}
