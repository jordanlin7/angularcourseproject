import { Directive, ElementRef, HostBinding, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @HostBinding('class.open') isOpen:boolean = false;

  // @HostListener('document:click', ['$event.target']) toggledropdown(event:HTMLElement) {
  //   this.isOpen = this.elemRef.nativeElement.contains(event)? !this.isOpen : false;
  // }

  /*
  HostListener前面傳的參數是html的事件名稱，後面['$event']是事件，就是給toggledropdown(event這個使用的event)
  */

  @HostListener('document:click', ['$event']) toggledropdown(event:Event) {
    this.isOpen = this.elemRef.nativeElement.contains(event.target)? !this.isOpen : false
  }

  constructor(private elemRef:ElementRef) {
  }

}
