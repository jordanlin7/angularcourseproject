export class Ingredient {
  // 這邊可以看到，跟recipe.model的寫法有點不一樣，
  // typescript提供了一種簡單的寫法，只要在constructor內的參數前加上public就能達到一樣的效果
  constructor(public name:string, public amount:number) {}

  /*
  public name:string;
  public amount:number;
  constructor(name:string, amount:number) {
    this.name = name;
    this.amount = amount;
  }
  */
}
