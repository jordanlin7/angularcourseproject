import { ShoppingListService } from './shopping-list.service';
import { Ingredient } from './../shared/ingredient.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {

  ingredients: Ingredient[] = [];

  private sub!: Subscription;

  constructor(private shoppinglistService : ShoppingListService) { }

  ngOnInit(): void {
    this.ingredients = this.shoppinglistService.getIngredients();

    // 這邊去訂閱事件，如果資料有新增就更新資料
    this.sub = this.shoppinglistService.ingredientadded.subscribe((value:Ingredient[])=> {
      this.ingredients = value;
    })
  }


  ngOnDestroy(): void {
    this.sub.unsubscribe();

  }

  onEditItem(index:number) {
    // 這邊透過service裡的startedEditing subject去發送點擊到的item index，之後會在shoppint-edit裡面去接收使用
    this.shoppinglistService.startedEditing.next(index);
  }

}
