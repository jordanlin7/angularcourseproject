import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatos', 10)
  ];

  ingredientadded = new Subject<Ingredient[]>();
  startedEditing = new Subject<number>();

  constructor() { }

  getIngredients() {
    return this.ingredients.slice();
  }

  getIngredient(index:number) {
    return this.ingredients[index];
  }

  addIngredient(newIngredient : Ingredient) {
    this.ingredients.push(newIngredient);
    // 在加入新的ingredient後，要有個事件去通知shopping-list.component，讓他知道資料更新了，要拿新的資料
    this.ingredientadded.next(this.ingredients.slice())
  }

  addIngredientsToShoppingList(newingredients : Ingredient[]) {
    this.ingredients.push(...newingredients);
    this.ingredientadded.next(this.ingredients.slice())
  }

  updateIngredient(index:number, newIngredient : Ingredient) {
    this.ingredients[index] = newIngredient;
    this.ingredientadded.next(this.ingredients.slice())
  }

  deleteIngredient(index:number) {
    this.ingredients.splice(index, 1);
    this.ingredientadded.next(this.ingredients.slice())
  }




}
