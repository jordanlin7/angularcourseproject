import { Subscription } from 'rxjs';
import { ShoppingListService } from './../shopping-list.service';
import { EventEmitter, OnDestroy } from '@angular/core';
import { Component, ElementRef, OnInit, Output, ViewChild } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  editMode = false;
  editItemIndex!: number;
  editItem!: Ingredient;

  subscription! : Subscription;

  @ViewChild('f') slForm! : NgForm;

  constructor(private shoppinglistService : ShoppingListService) { }

  ngOnInit(): void {
    this.subscription = this.shoppinglistService.startedEditing.subscribe((index:number)=> {
      this.editMode = true;
      this.editItemIndex = index;
      this.editItem = this.shoppinglistService.getIngredient(index);
      this.slForm.setValue({
        name: this.editItem.name,
        amount: this.editItem.amount
      })
    })


  }

  onAddIngredient(form : NgForm) {
    const value = form.value;
    const data:Ingredient = new Ingredient(value.name, value.amount);

    if(this.editMode) {
      this.shoppinglistService.updateIngredient(this.editItemIndex, data)

    }
    else {
      this.shoppinglistService.addIngredient(data);
    }

    form.reset();
    this.editMode = false;

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();

  }

  onClear() {
    this.slForm.reset();
    this.editMode = false;
  }

  onDelete() {
    this.shoppinglistService.deleteIngredient(this.editItemIndex);
    this.onClear();

  }

}
